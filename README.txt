
Description
-----------
This module offers simple support for the shortlink standard, currently only
for nodes.

Typically, you use the Pathauto module to automatically generate path aliases
for the content (nodes) you create (or maybe you enter aliases manually).
These are typically longer than the original "node/<nid>" paths.

This module uses those "node/<nid>" paths for short URLs. This of course
implies that your short URLs will only go offline if your site goes offline.
This module relies on the Global Redirect module to perform the redirects to
the aliased paths.

Note: the shortlink specification (http://microformats.org/wiki/rel-shortlink)
seems to have the best chance of ending up being the winner, as WordPress.com
(with millions of blogs) has started supporting this standard.


Dependencies
------------
* Global Redirect (http://drupal.org/project/globalredirect)


Installation
------------
1) Place this module directory in your "modules" folder (this will usually be
"sites/all/modules/"). Don't install your module in Drupal core's "modules"
folder, since that will cause problems and is bad practice in general. If
"sites/all/modules" doesn't exist yet, just create it.

2) Install the Global Redirect module, if you haven't installed it yet already.

3) Enable the module.
